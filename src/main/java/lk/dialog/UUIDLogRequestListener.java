package lk.dialog;

/**
 * Created by Malinda_07654 on 2016-11-30.
 */

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.UUID;

public class UUIDLogRequestListener implements ServletRequestListener {
    protected static final Logger LOGGER = Logger.getLogger(UUIDLogRequestListener.class);

    public void requestInitialized(ServletRequestEvent arg0) {

        MDC.put("RequestId", UUID.randomUUID());
        LOGGER.debug("REQUEST INIT " + MDC.get("RequestId"));

    }

    public void requestDestroyed(ServletRequestEvent arg0) {
        LOGGER.debug("REQUEST DESTROYED " + MDC.get("RequestId"));
        MDC.clear();
    }
}
