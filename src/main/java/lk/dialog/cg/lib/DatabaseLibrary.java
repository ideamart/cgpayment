package lk.dialog.cg.lib;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by Malinda on 7/10/2015.
 */
public class DatabaseLibrary {

    private static DataSource AppDatabaseSource=null;
    private static DataSource LogDatabaseSource=null;
    private static Logger logger;

    public DataSource getAppDatabaseSource() {
        return AppDatabaseSource;
    }

    public void setAppDatabaseSource(DataSource appDatabaseSource) {
        AppDatabaseSource = appDatabaseSource;
    }

    public DataSource getLogDatabaseSource() {
        return LogDatabaseSource;
    }

    public void setLogDatabaseSource(DataSource logDatabaseSource) {
        LogDatabaseSource = logDatabaseSource;
    }


    public DatabaseLibrary() {
        logger = Logger.getLogger(DatabaseLibrary.class);
        if (AppDatabaseSource == null || LogDatabaseSource == null)
            DatabaseFromConfig();
    }


    public void DatabaseFromConfig() {
        logger.info("Initializing Database from config");
        Properties props = new Properties();
        FileInputStream fis = null;
        try {
            Boolean loaded = false;
            try {
                props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/conf.properties"));
                loaded = true;
            } catch (Exception e) {
                logger.error("Config Load filed from App. trying to load from LIB: ");
            }
            if (!loaded)
                props.load(DatabaseLibrary.class.getResourceAsStream("/db.properties"));

            ComboPooledDataSource mysqlDataSource = new ComboPooledDataSource();
            mysqlDataSource.setJdbcUrl(props.getProperty("jdbc.ADMIN.url"));
            mysqlDataSource.setUser(props.getProperty("jdbc.ADMIN.username"));
            mysqlDataSource.setPassword(props.getProperty("jdbc.ADMIN.password"));
            mysqlDataSource.setMaxPoolSize(Integer.parseInt(props.getProperty("jdbc.ADMIN.maxPool")));
            mysqlDataSource.setMinPoolSize(Integer.parseInt(props.getProperty("jdbc.ADMIN.minPool")));
            mysqlDataSource.setMaxStatements(Integer.parseInt(props.getProperty("jdbc.ADMIN.maxStatements")));
            mysqlDataSource.setTestConnectionOnCheckout(Boolean.parseBoolean(props.getProperty("jdbc.ADMIN.testConnection")));
            AppDatabaseSource = mysqlDataSource;
            logger.info("App DB init success");


            ComboPooledDataSource mysqlDataSource2 = new ComboPooledDataSource();
            mysqlDataSource2.setJdbcUrl(props.getProperty("jdbc.LOG.url"));
            mysqlDataSource2.setUser(props.getProperty("jdbc.LOG.username"));
            mysqlDataSource2.setPassword(props.getProperty("jdbc.LOG.password"));
            mysqlDataSource2.setMaxPoolSize(Integer.parseInt(props.getProperty("jdbc.LOG.maxPool")));
            mysqlDataSource2.setMinPoolSize(Integer.parseInt(props.getProperty("jdbc.LOG.minPool")));
            mysqlDataSource2.setMaxStatements(Integer.parseInt(props.getProperty("jdbc.LOG.maxStatements")));
            mysqlDataSource2.setTestConnectionOnCheckout(Boolean.parseBoolean(props.getProperty("jdbc.LOG.testConnection")));
            LogDatabaseSource = mysqlDataSource2;
            logger.info("LOG DB init success");

        } catch (Exception e) {
            logger.error("DB Init Error : " + e.getMessage(), e);
        }

    }


}
