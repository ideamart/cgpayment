package lk.dialog.cg.lib;

import lk.dialog.cg.models.LogResult;
import lk.dialog.ideabiz.api.model.internal.adminapi.payment.PAYMENT_APP;
import lk.dialog.ideabiz.api.model.internal.adminapi.payment.PAYMENT_APP_CONFIG;
import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by malinda_07654 on 2/27/2016.
 */
public class PaymentLib {
    static DatabaseLibrary databaseLibrary = null;
    static Logger logger = Logger.getLogger(PaymentLib.class);

    public static PAYMENT_APP_CONFIG getPaymentApplicationByAMID(Integer AMID, String reasonCode) {

        String sqlQuery = "SELECT `REASON_CODE`,`payment_app`.* FROM `payment_config` , `payment_app` WHERE `APIMGR_ID` = ? AND `payment_config`.`ID`=`payment_app`.`PAYMENT_CONFIG_ID` ORDER BY `payment_app`.`DEFAULT` DESC LIMIT 0,1";

        if (reasonCode == null || reasonCode.equals(""))
            sqlQuery = "SELECT `REASON_CODE`,`payment_app`.* FROM `payment_config` , `payment_app` WHERE `APIMGR_ID` = ? AND `payment_config`.`ID`=`payment_app`.`PAYMENT_CONFIG_ID`  AND  `payment_config`.`REASON_CODE` = ? ORDER BY `payment_app`.`DEFAULT`  DESC LIMIT 0,1";


        Connection dbConnection = null;
        PAYMENT_APP_CONFIG payment_app_config = null;
        try {
            dbConnection = databaseLibrary.getAppDatabaseSource().getConnection();
            PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, AMID);

            if (reasonCode == null || reasonCode.equals(""))
                preparedStatement.setString(2, reasonCode);


            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                payment_app_config = new PAYMENT_APP_CONFIG();
                payment_app_config.setID(rs.getInt("ID"));
                payment_app_config.setAPPLICATION_ID(rs.getInt("APPLICATION_ID"));
                payment_app_config.setAM_ID(rs.getInt("APIMGR_ID"));
                payment_app_config.setMIN_AMOUNT(rs.getDouble("MIN_AMOUNT"));
                payment_app_config.setMAX_AMOUNT(rs.getDouble("MAX_AMOUNT"));
                payment_app_config.setPAYMENT_CONFIG_ID(rs.getInt("PAYMENT_CONFIG_ID"));
                payment_app_config.setDEFAULT(rs.getInt("DEFAULT"));
                payment_app_config.setCHECK_CLIENTCORELLATOR(rs.getInt("CHECK_CLIENTCORELLATOR"));
                payment_app_config.setREASON_CODE(rs.getString("REASON_CODE"));


            }

        } catch (SQLException e) {
            logger.error("Database Error :" + e.getMessage(),e);
        } catch (Exception e) {
            logger.error("Known Error :" + e.getMessage(),e);
        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return payment_app_config;
    }

    public static LogResult getLogByClientCorrelator(Integer applicationAMId, String cliendCorrelator){
        String sqlQuery = "SELECT msisdn,chargingtype,reasoncode,result_status,amount,dialog_ref,result_description,requested_status FROM `payment_transaction` WHERE `appid` = ? AND `client_correlator_hash` = MD5(?) ";


        Connection dbConnection = null;
        LogResult logResult = null;
        try {
            dbConnection = databaseLibrary.getLogDatabaseSource().getConnection();
            PreparedStatement preparedStatement = dbConnection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, applicationAMId);
            preparedStatement.setString(2, cliendCorrelator);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                logResult = new LogResult();

                logResult.setMsisdn(rs.getString("msisdn"));
                logResult.setChargingtype(rs.getString("chargingtype"));
                logResult.setReasoncode(rs.getString("reasoncode"));
                logResult.setResult_status(rs.getString("result_status"));
                logResult.setDialog_ref(rs.getString("dialog_ref"));
                logResult.setAmount(rs.getDouble("amount"));
                logResult.setResult_description(rs.getString("result_description"));
                logResult.setRequested_status(rs.getString("requested_status"));
                logResult.setMsisdn_type(rs.getString("msisdntype"));

            }

        } catch (SQLException e) {
            logger.error("Database Error :" + e.getMessage(),e);
        } catch (Exception e) {
            logger.error("Known Error :" + e.getMessage(),e);
        } finally {
            try {
                if (dbConnection != null && !dbConnection.isClosed())
                    dbConnection.close();
            } catch (SQLException e) {
            }
        }

        return logResult;

    }

    public PaymentLib() {
        databaseLibrary = new DatabaseLibrary();
    }
}
