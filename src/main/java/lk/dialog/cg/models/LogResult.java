package lk.dialog.cg.models;

/**
 * Created by malinda_07654 on 2/27/2016.
 */
public class LogResult {
    String msisdn;
    String chargingtype;
    String reasoncode;
    String result_status;
    String result_description;
    String requested_status;
    String dialog_ref;
    Double amount;
    String msisdn_type;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getChargingtype() {
        return chargingtype;
    }

    public void setChargingtype(String chargingtype) {
        this.chargingtype = chargingtype;
    }

    public String getReasoncode() {
        return reasoncode;
    }

    public void setReasoncode(String reasoncode) {
        this.reasoncode = reasoncode;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDialog_ref() {
        return dialog_ref;
    }

    public void setDialog_ref(String dialog_ref) {
        this.dialog_ref = dialog_ref;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }

    public String getRequested_status() {
        return requested_status;
    }

    public void setRequested_status(String requested_status) {
        this.requested_status = requested_status;
    }

    public String getMsisdn_type() {
        return msisdn_type;
    }

    public void setMsisdn_type(String msisdn_type) {
        this.msisdn_type = msisdn_type;
    }
}
